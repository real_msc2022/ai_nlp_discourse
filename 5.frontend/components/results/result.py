# put graphs here

import plotly.graph_objects as go
import plotly.express as px

# temp graph imports
### FORMAT DATASET INTO GRAPH FRIENDLY DATAFRAME ###
### IMPORT ###
import pandas as pd
import plotly.graph_objects as go
import networkx as nx

#import matplotlib.pyplot as plt

import re
from networkx.drawing.nx_agraph import graphviz_layout, to_agraph
import pygraphviz as pgv

import dash_cytoscape as cyto


class Result():
    
    def __init__(self, dtf):
        self.dtf = dtf


    def plot_graph(self,abscol,ordcol):
        ## main plots
        fig = go.Figure()
        fig.add_trace(go.Scatter(x=self.dtf[abscol], y=self.dtf[ordcol], mode='lines', name='data', line={"color":"black"}))
        ## add slider
        fig.update_xaxes(rangeslider_visible=True)    
        ## set background color
        fig.update_layout(plot_bgcolor='white', autosize=False, width=1000, height=550)        
        return fig


    def plot_analysis(self):
        '''
        mockup graph to use as a placeholder
        input none
        output fig
        '''
        ## main plots
        fig = go.Figure(go.Indicator(
        mode = "gauge+number",
        value = 6.5,
        domain = {'x': [0, 1], 'y': [0, 1]},
        title = {'text': "Efficiency"}))
        return fig


    def plot_analysis2(self):
        ## main plots
        fig = go.Figure()
        # add traces 
        data_canada = px.data.gapminder().query("country == 'Canada'")
        fig = px.bar(data_canada, x='year', y='pop') 
        ## add slider
        fig.update_xaxes(rangeslider_visible=True)    
        ## set background color
        fig.update_layout(plot_bgcolor='white', autosize=False, width=1000, height=550)        
        return fig
        


    def create_rnd_graph(self):
        G1 = nx.random_geometric_graph(150, 0.120)
        return G1


    def create_graph(self):
        '''
        purpose create a network graph using networkx library
        input none (csv file harcoded as input)
        output graph nx
        '''
        ### CREATE DATASET ###
        #parse gare data

        G2= nx.Graph()
        with open("../3.dataset/dataset_test/timetables.csv",'r') as readed:
            for line in readed.readlines()[1:]:
                split = line.split("\t")
                #print(split)
                gares = split[1].split(" - ")
                #print(gares)
                dist = re.findall(r'\d+', split[2])
                num_dist = int(dist[0])
                #print(num_dist)
                
                #
                #
                #
                
                try:
                    # NODE 0
                    G2.add_node(gares[0])
                    #    db.execute(f"Insert into gare(nom_gare) values (\"{gares[0]}\");")
                    #print(f"Insert into gare(nom_gare1) values (\"{gares[0]}\");")
                except:
                    # NODE ALREADY EXISTS
                    print(f"La gare {gares[0]} existe déja")
                try:
                    # NODE 1
                    G2.add_node(gares[1])
                    #print(f"Insert into gare(nom_gare2) values (\"{gares[1]}\");")
                    #    db.execute(f"Insert into gare(nom_gare) values (\"{gares[1]}\");")
                except:
                    print(f"La gare {gares[1]} existe déja")
                try:
                    # EDGE 0 TO 1
                    G2.add_edge(gares[0], gares[1], length=num_dist)
                    #    db.execute(f"Insert into associe(id_gare,id_gare1) values ( \
                    #    (select id_gare from gare where nom_gare=\"{gares[0]}\"), \
                    #    (select id_gare from gare where nom_gare=\"{gares[1]}\"));")
                except:
                    # EDGE EXISTING ALREADY
                    print(f"L'assotiation {gares[0]}/{gares[1]} existe déja")

        #basic graph plotting            
        #nx.draw(G2, with_labels=True)

        #basic graph plotting v2
        #pos = nx.spring_layout(G2, weight='length')
        #nx.draw(G2, pos)
        #nx.draw_networkx_edge_labels(G2, pos)
        #plt.draw()
        #plt.show

        # print dot graph
        '''
        R = to_agraph(G2)
        #print(R)
        R.layout('dot')
        R.draw('rail1.png')
        '''
        return G2


    def plot_graph(self, G):
        '''
        goal build a graph object
        input graph
        output node_trace, edge_trace in a list
        '''
        edge_x = []
        edge_y = []
        for edge in G.edges():
            #print(G.nodes[edge[0]]['pos'])
            x0, y0 = G.nodes[edge[0]]['pos']
            x1, y1 = G.nodes[edge[1]]['pos']
            edge_x.append(x0)
            edge_x.append(x1)
            edge_x.append(None)
            edge_y.append(y0)
            edge_y.append(y1)
            edge_y.append(None)

        edge_trace = go.Scatter(
            x=edge_x, y=edge_y,
            line=dict(width=0.5, color='#888'),
            hoverinfo='none',
            mode='lines')

        node_x = []
        node_y = []
        for node in G.nodes():
            x, y = G.nodes[node]['pos']
            node_x.append(x)
            node_y.append(y)

        node_trace = go.Scatter(
            x=node_x, y=node_y,
            mode='markers',
            hoverinfo='text',
            marker=dict(
                showscale=True,
                # colorscale options
                #'Greys' | 'YlGnBu' | 'Greens' | 'YlOrRd' | 'Bluered' | 'RdBu' |
                #'Reds' | 'Blues' | 'Picnic' | 'Rainbow' | 'Portland' | 'Jet' |
                #'Hot' | 'Blackbody' | 'Earth' | 'Electric' | 'Viridis' |
                colorscale='YlGnBu',
                reversescale=True,
                color=[],
                size=10,
                colorbar=dict(
                    thickness=15,
                    title='Node Connections',
                    xanchor='left',
                    titleside='right'
                ),
                line_width=2))
        graph_stuff = [node_trace, edge_trace]
        return graph_stuff


    def color_graph(self, H):
        '''
        overload color based on number of connections
        input graph
        output node_trace
        '''
        node_adjacencies = []
        node_text = []
        for node, adjacencies in enumerate(H.adjacency()):
            node_adjacencies.append(len(adjacencies[1]))
            node_text.append('# of connections: '+str(len(adjacencies[1])))

        node_trace.marker.color = node_adjacencies
        node_trace.text = node_text
        return node_trace


    def network_graph(self, graph_stuff):
        node_trace = graph_stuff[0]
        edge_trace = graph_stuff[1]
        fig = go.Figure(data=[edge_trace, node_trace],
            layout=go.Layout(
                title='<br>Network graph made with Python',
                titlefont_size=16,
                showlegend=False,
                hovermode='closest',
                margin=dict(b=20,l=5,r=5,t=40),
                annotations=[ dict(
                    text="Python code: <a href='https://plotly.com/ipython-notebooks/network-graphs/'> https://plotly.com/ipython-notebooks/network-graphs/</a>",
                    showarrow=False,
                    xref="paper", yref="paper",
                    x=0.005, y=-0.002 ) ],
                xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                yaxis=dict(showgrid=False, zeroline=False, showticklabels=False))
                )
        return fig


    def create_sample_graph(self):
        '''
        sample func to build graph
        '''
        G = nx.random_geometric_graph(200, 0.125)
 
        ### GRAPH PLOTING ###


        #
        # Create Edges
        # Add edges as disconnected lines in a single trace and nodes as a scatter trace
        #


        edge_x = []
        edge_y = []
        print(G)
        list(G.edges())
        for edge in G.edges():
            #print(G2.nodes[edge[0]][0]['pos'])
            x0, y0 = G.nodes[edge[0]]['pos']
            x1, y1 = G.nodes[edge[1]]['pos']
            edge_x.append(x0)
            edge_x.append(x1)
            edge_x.append(None)
            edge_y.append(y0)
            edge_y.append(y1)
            edge_y.append(None)

        edge_trace = go.Scatter(
            x=edge_x, y=edge_y,
            line=dict(width=0.5, color='#888'),
            hoverinfo='none',
            mode='lines')

        node_x = []
        node_y = []
        for node in G.nodes():
            x, y = G.nodes[node]['pos']
            node_x.append(x)
            node_y.append(y)

        node_trace = go.Scatter(
            x=node_x, y=node_y,
            mode='markers',
            hoverinfo='text',
            marker=dict(
                showscale=True,
                # colorscale options
                #'Greys' | 'YlGnBu' | 'Greens' | 'YlOrRd' | 'Bluered' | 'RdBu' |
                #'Reds' | 'Blues' | 'Picnic' | 'Rainbow' | 'Portland' | 'Jet' |
                #'Hot' | 'Blackbody' | 'Earth' | 'Electric' | 'Viridis' |
                colorscale='YlGnBu',
                reversescale=True,
                color=[],
                size=10,
                colorbar=dict(
                    thickness=15,
                    title='Node Connections',
                    xanchor='left',
                    titleside='right'
                ),
                line_width=2))


        #
        #Color Node Points
        #Color node points by the number of connections.
        #


        node_adjacencies = []
        node_text = []
        for node, adjacencies in enumerate(G.adjacency()):
            node_adjacencies.append(len(adjacencies[1]))
            node_text.append('# of connections: '+str(len(adjacencies[1])))

        node_trace.marker.color = node_adjacencies
        node_trace.text = node_text


        #
        # Create Network Graph
        #
        #


        fig = go.Figure(data=[edge_trace, node_trace],
                    layout=go.Layout(
                        title='<br>Railway Network',
                        titlefont_size=16,
                        showlegend=False,
                        hovermode='closest',
                        margin=dict(b=20,l=5,r=5,t=40),
                        annotations=[ dict(
                            text="818 stations and 1572 sections",
                            showarrow=False,
                            xref="paper", yref="paper",
                            x=0.005, y=-0.002 ) ],
                        xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                        yaxis=dict(showgrid=False, zeroline=False, showticklabels=False))
                        )
        return fig

    def main_graph(self):
        '''
        build an interactive graph: from graph to trace and finally fig
        input none
        output fig
        '''
        graph = create_graph()
        plotting_trace = plot_graph(graph)
        plotting_trace[0] = color_graph(plotting_trace)
        fig = network_graph(plotting_trace)
        return fig


    def cytograph(G):
        '''
        func to build a fig of a network graph from an existing graph
        input G from nx
        output fig to show
        '''
        # init
        fig=0
        nodes = set()
        cy_edges = []
        cy_nodes = []

        # store
        
        
        #build
        


        #style - sounds optional, we like good ol' html
        stylesheet = []
        return fig


    def dj_path(self, G):
        print(nx.dijkstra_path(G, 'Gare de Biel', 'Gare de Meroux'))