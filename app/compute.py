import speech_recognition as sr
import spacy
import feedparser

from sklearn.linear_model import LogisticRegression
import pandas as pd
import speech_recognition as sr
import spacy
from spacy import displacy

#convert audio to the txt
filename = "app/machine-learning_speech-recognition_16-122828-0002.wav"
# initialize the recognizer
r = sr.Recognizer()
# open the file
with sr.AudioFile(filename) as source:
    # listen for the data (load audio to memory)
    audio_data = r.record(source)
    # recognize (convert from speech to text)
    text = r.recognize_google(audio_data)
    print(text)


#https://www.thepythoncode.com/article/using-speech-recognition-to-convert-speech-to-text-python
nlp = spacy.load('fr_core_news_sm')
url = "https://www.france24.com/fr/rss"
# Fetch Info From RSS
# pip  install feedparser
f = feedparser.parse(url)
f.keys()
ex1="hello word i' m tired"
ex2 = f.entries[1]['summary']
docx = nlp(ex1)
docx = nlp(ex2)
for token in docx:
    if token.is_stop != True:
        print(token.text)
#"_________________________________________________________________________________________________________________________________________"

import torch
from transformers import QuestionAnsweringPipeline
from transformers import CamembertForQuestionAnswering
from transformers import CamembertTokenizer


QA_model = 'illuin/camembert-base-fquad'
CamTokQA = CamembertTokenizer.from_pretrained(QA_model)
CamQA = CamembertForQuestionAnswering.from_pretrained(QA_model)
#%%

context ='Bonjour, je voudrais partir de Lille pour aller Paris. Est ce que vous etes dispo?'
# Allocate a pipeline for question-answering
question_answerer = QuestionAnsweringPipeline(model=CamQA,
                                         tokenizer=CamTokQA)
depart = question_answerer({
     'question': 'quelle est la ville de départ ?',
     'context': context
 })
arrive = question_answerer({
     'question': 'quelle est la ville de destination ?',
     'context': context
 })
print(depart)
print(arrive)
#------------------------------------------------------------------------------------------------------------------------------------


nlp = spacy.load("fr_core_news_md")


@Language.factory("language_detector_fr2")
def get_lang_detector(nlp, name):
    return LanguageDetector()


nlp.add_pipe('language_detector_fr2', last=true)
