### Devops tools

#### Tools for python/Julia to ease microservice deployment
Here is a solution easily scalable into an hosrizontal cluster such as kubernetes

#### Pros/Cons Python
Unlike NodeJs no package manager is imposed
Unlike NodeJs dependancies have no scope, by defualt is global. Unless using docker or anaconda/conda

- Challenges
1.  Multiple packages manager available with different ecosystem (pip/conda)
2.  Prject setup is not crystal clear, unless specified in a Readme
3.  temptation to run `sudo pip install`
4.  Clutters the doc a bit
5.  Machine to machine compatibility is not guaranteed

- Pros
1. more flexibility
2. NPM has a lot of complex bugs within `npm install` in comparison
3. no dependancy module like node_modules with ownership to manage (happen often with root user or running install with superuser rights `sudo npm install`)

#### On the go solution: python shell
copy paste snippet into shell-python3 and hope.

#### Mixed bag solution: Jupyter Notebook
Notebook has it's own "Kernel". Also compatible with R, Julia and Jshell(Java)... but keeping architecture as monolithic, filesystem management is more complex.

#### Good solution:
Run code isaolated in a Docker container. 3 working startegies:
- `docker run` no filesystem link, manual code copy paste into interface(terminal/notebook/other)
- `docker copy` copy a snapshot of file into the container. Working without backup, if docker is downed data would be lost.
- `docker run -v HOST:GUEST` filesystem is mounted into the docker. Can be Read+Write (both way) or RO

**Best practice:** Rembering to list dependancies into a file `requirements.txt`

#### Uses case Python DEV
1. the snipept Notebook (the famous)
    - `docker run -it --name notebook1 --network host jupyter/datascience-notebook`
    Can change image if needed. ie can use nvidia based ones.

2. Deployment a "Production" like micro-service
    - `docker build -t pythonprod1 -f prod/PythonProd.Dockerfile`
    When build is working,it's fine. When run is working it's perfect. Best practice is to record all parameters and environment option into a docker-compose file (risk of forgetting stuuf over tiime).
    Dependancies should be updated.
    Best use practice, define versions to use, ie `FROM python:3.9`
    - `docker run -it --name notebook1 --network host pythonprod1` for first run it's recommended to create a name with a number, in order to increment version.
    - `docker exec -it notebook1` to access the shell into docker, once it has started
    - `docker logs notebook1` in case of issues, but one can also recover logs using docker cp.

3. The docker-compose (to avoid typing all parameters by hand)
    - `docker-compose up --build`
    code and tools are up.
    choose image by modifiying image field into `docker-compose.yml`: either select a docker image from on dockerhub, or define path  to a dockerfile, use a token for a private contianer hoster in gitlab with docker login **registry.gitlab.com** (gitlab has nice additions)


**Conclusion**, the more time on docker/Dockerfile/docker-compose the faster the micro-service clusteried easily (k8s)

- `docker login https:registry.gitlab.com`
- `docker tag blabla [todo]  mytag1`
- `docker push mytag1`