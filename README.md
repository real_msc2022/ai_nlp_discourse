# AI_nlp_discourse

nlp project based on discourse

## Models definition
## Concepts definition
List of all mathematical concept
### Dataframe
It is a type of data storage where data is organized in tables and structured (similar to SQL)
### Graph theory/network
A graph **G** has nodes and edges
![graph with plotly and pyplot](2.documentation/Screenshot_2022-01-06_at_17.29.26.png)
![dot graph](2.documentation/Screenshot_2022-01-25_at_23.48.29.png)
#### Pathfinding tree
Type of problem where we know the know and we want to know a path respecting criterias, ie shortest distance often.
### Markov Model
WHAT IS A MARKOV MODEL?
- A Markov Model is a stochastic model which models
temporal or sequential data, i.e., data that are ordered.
- It provides a way to model the dependencies of current
information (e.g. weather) with previous information.
- It is composed of states, transition scheme between states,
and emission of outputs (discrete or continuous).
- Several goals can be accomplished by using Markov models:
	- Learn statistics of sequential data.
	- Do prediction or estimation.
	- Recognize patterns

### Hidden Markov model
A Hidden Markov Model, is a stochastic model where
the states of the model are hidden. Each state can emit
an output which is observed.
- Imagine: You were locked in a room for several days
and you were asked about the weather outside. The
only piece of evidence you have is whether the person
who comes into the room bringing your daily meal is
carrying an umbrella or not.
  - What is hidden? Sunny, Rainy, Cloudy
  - What can you observe? Umbrella or Not

#the nlp section in three steps

## Voice recognition and recording  
### SpeechRecognition
Why ?
- it is a package that stands out in terms of ease of use.
- Recognizing speech requires audio input, and SpeechRecognition makes retrieving this input really easy. 
Instead of having to build scripts for accessing microphones and processing audio files from scratch,
SpeechRecognition will have you up and running in just a few minutes.
- The flexibility and ease-of-use of the SpeechRecognition package make it an excellent choice for our project.

How does it work ?


## language detection
### Spacy => LanguageDetector
we can detect the language to use by using LanguageDetector module.

## NLP

### camembert-ner
[camembert-ner] is a NER model that was fine-tuned from camemBERT on wikiner-fr dataset. Model was trained on wikiner-fr dataset (~170 634 sentences). 
Model was validated on emails/chat data and overperformed other models on this type of data specifically. 
In particular the model seems to work better on entity that don't start with an upper case.

Training data was classified as follow:

| Abbreviation | Description |
| -----------  | ----------- |
|O	           | Outside of a named entity|
|MISC	       | Miscellaneous entity |
|PER	       | Person’s name |
|ORG           | Organization |
|LOC	       | Location |

In our project we use camembert-ner to define the locality, we extract with this algorithm the different cities in the sentence, that we will store in a list, this list will be used in other function later.

Why camembert-ner ?

we chose camembert-ner for its precision of 0.89 (for the locality) 

### spaCy + Stanza (formerly StanfordNLP)

When you call nlp on a text, spaCy first tokenizes the text to produce a Doc object. The Doc is then processed in several different steps – this is also referred to as the processing pipeline. 
The pipeline used by the trained pipelines typically include a tagger, a lemmatizer, a parser and an entity recognizer. Each pipeline component returns the processed Doc,
which is then passed on to the next component.

![nlp](0.subject/pipeline.svg)

This Stanza package (formerly StanfordNLP) library, so you can use Stanford's models in a spaCy pipeline. 
The Stanford models achieved top accuracy in the CoNLL 2017 and 2018 shared task, which involves tokenization, part-of-speech tagging, morphological analysis, lemmatization and labeled dependency parsing in 68 languages. As of v1.0, Stanza also supports named entity recognition for selected languages.

Using this wrapper, you'll be able to use the following annotations:

![token](0.subject/token.png)

we chose spaCy + Stanza for its detailed annotation, 
because we could extract the verbs and determine the arrival and the destination 

