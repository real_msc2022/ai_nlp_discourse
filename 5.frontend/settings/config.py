
import os

## App settings
name = "dashboard data"

host = "0.0.0.0"

port = int(os.environ.get("PORT", 6001))

debug = True

contacts = "#"

code = "#"

tutorial = "#"

fontawesome = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'



## File system
root = os.path.dirname(os.path.dirname(__file__)) + "/"



## DB
