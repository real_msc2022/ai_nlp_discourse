# Where AI magic occurs
# not used for basic app
import pandas as pd
import numpy as np
from scipy import optimize



class Model():
    
    def __init__(self, dtf):
        self.dtf = dtf


    @staticmethod
    def f(X, c, k, m):
        y = c / (1 + np.exp(-k*(X-m)))
        return y


    @staticmethod
    def fit_parametric(X, y, f, p0):
        model, cov = optimize.curve_fit(f, X, y, maxfev=10000, p0=p0)
        return model


    @staticmethod
    def forecast_parametric(model, f, X):
        preds = f(X, model[0], model[1], model[2])
        return preds


    @staticmethod
    def generate_indexdate(start):
        index = pd.date_range(start=start, periods=30, freq="D")
        index = index[1:]
        return index


    @staticmethod
    def add_diff(dtf):
        ## create delta columns
        ## fill Nas
        ## interpolate outlier


    def forecast(self):
        ## fit
        ## forecast
        ## create dtf
        ## add diff