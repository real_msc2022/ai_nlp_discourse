# interactive data app
## Setup
```
python run.py
```
or with docker
```
chmod u+x localdockerbuild.sh
./localdockerbuild.sh
```

## TODO
* add api client code (previously with csv)
* split app into components
* dicuss and implement about useful User Experience flow
* conda env files for native development
* docker dotenv integration
* update documentation
* one day add testing
* some real world CD
