###############################################################################
#       dashboard
###############################################################################


# Setup
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import pandas as pd

from settings import config, about
from components.datas.data import Data
#from components.models.model import Model
from components.results.result import Result

import dash_cytoscape as cyto

# Read data
data = Data()
data.get_dataset()


# App Instance
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.LUX, config.fontawesome])


# Navbar
navbar = dbc.Nav(className="nav nav-pills", children=[
    ## logo/home
    dbc.NavItem(html.Img(src=app.get_asset_url("logo.PNG"), height="40px")),
    ## about
    dbc.NavItem(html.Div([
        dbc.NavLink("About", href="/", id="about-popover", active=False),
        dbc.Popover(id="about", is_open=False, target="about-popover", children=[
            dbc.PopoverHeader("How it works"), dbc.PopoverBody(about.txt)
        ])
    ])),
    ## links
    dbc.DropdownMenu(label="Links", nav=True, children=[
        dbc.DropdownMenuItem([html.I(className="fa fa-linkedin"), "  Contacts"], href=config.contacts, target="_blank"), 
        dbc.DropdownMenuItem([html.I(className="fa fa-github"), "  Code"], href=config.code, target="_blank"),
        dbc.DropdownMenuItem([html.I(className="fa fa-medium"), "  Tutorial"], href=config.tutorial, target="_blank"),
    ])
])


# Input
inputs = dbc.FormGroup([
    html.H4("Select usecase"),
    dcc.Dropdown(id="dropdown", 
    options=[
        {'label': 'travel resolver', 'value': 'travel' },
        {'label': 'nlp only', 'value': 'nlp' },
        {'label': 'speech recognition only', 'value': 't2s' },
        {'label': 'pathfinding only', 'value': 'pathfinder' },
        {'label': 'test_panel', 'value': 'test' },
    ],
    value="travel",
    )
]) 


###############################################################################
#   Cyto component
###############################################################################

# prepare data
edges = pd.DataFrame.from_dict({'from':['earthquake', 'earthquake', 'burglary', 'alarm', 'alarm'],
                               'to': ['report', 'alarm', 'alarm','John Calls', 'Mary Calls']})
nodes = set()

cy_edges = []
cy_nodes = []

for index, row in edges.iterrows():
    source, target = row['from'], row['to']

    if source not in nodes:
        nodes.add(source)
        cy_nodes.append({"data": {"id": source, "label": source}})
    if target not in nodes:
        nodes.add(target)
        cy_nodes.append({"data": {"id": target, "label": target}})

    cy_edges.append({
        'data': {
            'source': source,
            'target': target
        }
    })


stylesheet = [
    {
        "selector": 'node', #For all nodes
        'style': {
            "opacity": 0.9,
            "label": "data(label)", #Label of node to display
            "background-color": "#07ABA0", #node color
            "color": "#008B80" #node label color
        }
    },
    {
        "selector": 'edge', #For all edges
        "style": {
            "target-arrow-color": "#C5D3E2", #Arrow color
            "target-arrow-shape": "triangle", #Arrow shape
            "line-color": "#C5D3E2", #edge color
            'arrow-scale': 2, #Arrow size
            'curve-style': 'bezier' #Default curve-If it is style, the arrow will not be displayed, so specify it
    }
}]


###############################################################################
#   End Cyto component
###############################################################################

# App Layout
app.layout = dbc.Container(fluid=True, children=[
    ## Top
    html.H1(config.name, id="nav-pills"),
    navbar,
    html.Br(),html.Br(),html.Br(),

    ## Body
    dbc.Row([
        ### input + panel
        dbc.Col(md=3, children=[
            inputs, 
            html.Br(),html.Br(),html.Br(),
            dcc.Tabs(id="context-panel", className="nav nav-pills", value="info", children=[
                dcc.Tab(label="INFO", value="info"),
                dcc.Tab(label="PF", value="pf"),
                dcc.Tab(label=" ", value="test"),
            ]),
            html.Div(id='tabs-content')
        ]),
        ### plots
        dbc.Col(md=9, children=[
            dbc.Col(html.H4("data dashboard"), width={"size":6,"offset":3}), 
            dbc.Tabs(className="nav nav-pills", children=[
                dbc.Tab(dcc.Graph(id="plot-graph"), label="graph"),
                dbc.Tab(html.Div([
                        dcc.Dropdown(
                            id='dropdown-layout',
                            options=[
                                {'label': 'random',
                                'value': 'random'},
                                {'label': 'grid',
                                'value': 'grid'},
                                {'label': 'circle',
                                'value': 'circle'},
                                {'label': 'concentric',
                                'value': 'concentric'},
                                {'label': 'breadthfirst',
                                'value': 'breadthfirst'},
                                {'label': 'cose',
                                'value': 'cose'}
                            ], value='random'
                        ),
                        html.Div(children=[
                            cyto.Cytoscape(
                                id='cytoscape',
                                elements=cy_edges + cy_nodes,
                                style={
                                    'height': '95vh',
                                    'width': '100%'
                                },
                                stylesheet=stylesheet
                            )
                        ])
                    ]),
                label="cyto"),

                #dcc.Tab(label="GRAPH", value="graph"),
                #dcc.Tab(label="ANALYSIS", value="analysis"),
            ]),
            #html.Div(id='graphs-content')
        ])
        
    ])
])


# Python functions for about navitem-popover
@app.callback(output=Output("about","is_open"), inputs=[Input("about-popover","n_clicks")], state=[State("about","is_open")])
def about_popover(n, is_open):
    if n:
        return not is_open
    return is_open


@app.callback(output=Output("about-popover","active"), inputs=[Input("about-popover","n_clicks")], state=[State("about-popover","active")])
def about_active(n, active):
    if n:
        return not active
    return active


# Python function to plot analysis
@app.callback(output=Output("plot-graph","figure"), inputs=[Input("dropdown","value")]) 
def plot_analysis_callback(dropdown):
    #print("trigger callback graph2")
    #df = data.get_coin(dropdown, 'day')
    #result = Result(df)
    #return Result.main_graph(dropdown)
    
    #graph = Result.create_rnd_graph(dropdown)
    #plotting_trace = Result.plot_graph(dropdown, graph)
    #plotting_trace[0] = Result.color_graph(dropdown, plotting_trace)
    #fig = Result.network_graph(dropdown, plotting_trace)
    
    fig = Result.create_sample_graph(dropdown) 
    return fig



@app.callback(Output('cytoscape', 'layout'),
              [Input('dropdown-layout', 'value')])
def update_cytoscape_layout(layout):
    return {'name': layout}


# Python function to plot graphs
"""
@app.callback(output=Output("graphs-content","figure"), inputs=[Input("plotting-zone","value")]) 
def plot_graph_callback(tab):
    print("trigger callback graphs plot")

    zone1 = html.Div([
        Result.plot_analysis2()
    ])

    zone2 = html.Div([
        Result.plot_analysis()
    ])

    if tab == 'graph':
        return zone1
    elif tab == 'analysis':
        return zone2
"""


# Python function to render output panel
@app.callback(output=Output("tabs-content","children"), inputs=[Input("context-panel","value")])
def render_output_panel(tab):

    peak_color = "grey"
    info_panel = html.Div([
        dbc.Card(body=True, className="text-white bg-primary", children=[
            
            html.H6("start point:", style={"color":"white"}),
            html.H3("Toulouse", style={"color":"white"}),
            
            html.H6("destination:", style={"color":peak_color}),
            html.H3("Bordeaux", style={"color":peak_color}),
            
            html.H6("travel time:", style={"color":"white"}),
            html.H3("140 minutes", style={"color":"white"}),

        ])
    ])

    pathfinder_panel = html.Div([
        dbc.Card(body=True, className="text-white bg-primary", children=[
            html.H3('User can modify advanced variables, basic UI for pathfinding FROM/TO ', style={"color":"white"}),
            dcc.Input(
                id="pf_from".format("text"),
                type="text",
                placeholder="from".format("text"),
            ),
            dcc.Input(
                id="pf_dest".format("text"),
                type="text",
                placeholder="destination".format("text"),
            ),
            html.Button('Submit', id='submit-val', n_clicks=0),
        ])
    ])

    test_panel = html.Div([
        dbc.Card(body=True, className="text-white bg-primary", children=[
            html.H3('Here is test UI panel where cmd can be hardwired ', style={"color":"white"}),

            html.Button('Speak_test', id='speak_test', n_clicks=0),

            dcc.Input(
                id="nlp_test".format("text"),
                type="text",
                placeholder="NLP test".format("text"),
            ),
        ])
    ])

    if tab == 'info':
        return info_panel
    elif tab == 'pf':
        return pathfinder_panel
    elif tab == 'test':
        return test_panel



'''
@app.callback(
    Output('container-button-basic', 'children'),
    Input('submit-val', 'n_clicks'),
    State('input-on-submit', 'value')
)
def update_output(n_clicks, value):
    return 'The input value was "{}" and the button has been clicked {} times'.format(
        value,
        n_clicks
    )
'''

