CREATE TABLE Gare(
   id_gare VARCHAR(50),
   nom_gare VARCHAR(50),
   position_gps TEXT,
   PRIMARY KEY(id_gare)
);

CREATE TABLE associe(
   id_gare VARCHAR(50),
   id_gare_1 VARCHAR(50),
   distance DOUBLE,
   PRIMARY KEY(id_gare, id_gare_1),
   FOREIGN KEY(id_gare) REFERENCES Gare(id_gare),
   FOREIGN KEY(id_gare_1) REFERENCES Gare(id_gare)
);